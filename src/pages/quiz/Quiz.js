import React, { useState } from "react";
import { Container, makeStyles, Typography } from "@material-ui/core";
import Questions from "../../components/Quiz/Questions";
import { Divider } from "@material-ui/core";
import UserInfo from "../../components/UserInfo/UserInfo";
import Result from "../../components/Quiz/Result";
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied";
import { Link } from "react-router-dom";
import Button from "@material-ui/core/Button";
import useUserAchievements from "../../hooks/useUserAchievements";
import useUserProducts from "../../hooks/useUserProducts";

const dummyData = {
  quizTitle: "Encuentra el regalo perfecto para ti!",
  answers: [
    {
      id: "1",
      option: "Experiencias",
      optionDescription:
        "Las experiencias son lo tuyo.  Nuestras recomendaciones para ti: ",
      showMore: "https://canjeonline-cmrpuntos.puntospoint.com/",
      showMoreText: "Ver más experiencias",
      products: [
        {
          image:
            "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/284/original/Calugas_Experiencias_Food_brunella-_4000.jpg",
          text: "4.000 pesos en Brunella",
          cmrPoints: 1000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/7fea637fd6d02b8f0adf6f7dc36aed93",
          expireDate: "31/12/2020",
        },
        {
          image:
            "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/078/original/Calugas_Experiencias_Food_barrachalaca-_22000.jpg",
          text: "22.000 pesos en Barra Chalaca",
          cmrPoints: 5000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/a40511cad8383e5ae8ddd8b855d135da",
          expireDate: "31/12/2020",
        },
        {
          image:
            "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/083/original/Calugas_Experiencias_Food_MU_22000.jpg",
          text: "22.000 pesos en MUU",
          cmrPoints: 5000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/9a4400501febb2a95e79248486a5f6d3",
          expireDate: "31/12/2020",
        },
      ],
    },
    {
      id: "2",
      option: "Artículos",
      optionDescription: "Nuestras mejores recomendaciones para tí",
      showMore: "https://tienda.cmrpuntos.cl/",
      showMoreText: "Ver más artículos",
      products: [
        {
          image:
            "https://i.linio.com/p/9a89ea668c97630593393cf025f0f517-zoom.webp",
          text: "Sandwichera Black RSA-6150a",
          cmrPoints: 4000,
          url:
            "https://tienda.cmrpuntos.cl/p/sandwichera-black-rsa-6150-opkk7r?qid=cc628690dd9ff2767bf0215a88a16609&oid=RE320HL1APMMALACL&position=2&sku=RE320HL1APMMALACL",
          expireDate: null,
        },
        {
          image:
            "https://falabella.scene7.com/is/image/Falabella/7981509?wid=200&amp;hei=200&amp;qlt=70",
          text: "Tostador RTO-CROSTINO100",
          cmrPoints: 5000,
          url:
            "https://tienda.cmrpuntos.cl/p/tostador-rto-crostino100-tnl63b?qid=a29cf8eac1bdb7108f5fd61cffbe7126&oid=RE320HL18W8GKLACL&position=3&sku=RE320HL18W8GKLACL",
          expireDate: null,
        },
        {
          image:
            "https://falabella.scene7.com/is/image/Falabella/881459287?wid=200&hei=200",
          text: "Hervidor Recco",
          cmrPoints: 5000,
          url:
            "https://tienda.cmrpuntos.cl/p/hervidor-rhe-17ss2-recco-rhe-17ss2-vb4il3?qid=61f174ff86d1ae7fd7a8ed847f016d5c&oid=RE320HL0C9YGELACL&position=4&sku=RE320HL0C9YGELACL",
          expireDate: null,
        },
      ],
    },
    {
      id: "3",
      option: "Gift Card",
      optionDescription: "opciones disponible para el canje",
      showMore: "https://canjeonline-cmrpuntos.puntospoint.com/",
      showMoreText: "Ver más experiencias",
      products: [
        {
          image:
            "https://i.linio.com/p/9a89ea668c97630593393cf025f0f517-zoom.webp",
          text: "Gift Card de $8.000 multiformato",
          cmrPoints: 5000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/6709e8d64a5f47269ed5cea9f625f7ab",
          expireDate: "31/07/2021",
        },
        {
          image:
            "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/078/original/Calugas_Experiencias_Food_barrachalaca-_22000.jpg",
          text: "22.000 pesos en Barra Chalaca",
          cmrPoints: 5000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/a40511cad8383e5ae8ddd8b855d135da",
          expireDate: "31/12/2020",
        },
        {
          image:
            "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/083/original/Calugas_Experiencias_Food_MU_22000.jpg",
          text: "22.000 pesos en MUU",
          cmrPoints: 5000,
          url:
            "https://canjeonline-cmrpuntos.puntospoint.com/experiences/9a4400501febb2a95e79248486a5f6d3",
          expireDate: "31/12/2020",
        },
      ],
    },
  ],
  questions: [
    {
      id: "1",
      text: "¿Que prefieres?",
      options: [
        {
          id: "1",
          answerId: "2",
          option: "Canje de puntos por Artículos",
          img: "",
        },
        {
          id: "2",
          answerId: "1",
          option: "Canje de puntos por Experiencias",
          img: "",
        },
        { id: "3", answerId: "3", option: "No lo sé", img: "" },
      ],
    },
    {
      id: "1",
      text: "¿Cómo sería tu cena perfecta?",
      options: [
        { id: "1", answerId: "3", option: "No lo sé" },
        {
          id: "2",
          answerId: "1",
          option: "Afuera en mi restaurant favorito",
          img: "",
        },
        {
          id: "3",
          answerId: "2",
          option: "En casa, preparada por mi",
          img: "",
        },
      ],
    },
    {
      id: "1",
      text: "Te defines como:",
      options: [
        { id: "1", answerId: "3", option: "No lo sé", img: "" },
        { id: "2", answerId: "2", option: "Una persona conservadora", img: "" },
        {
          id: "3",
          answerId: "1",
          option: "Una persona a la que le gusta probar cosas nuevas",
          img: "",
        },
      ],
    },
  ],
};

const genDummyEvents = (number) => {
  let arr = [];
  for (let i = 0; i < number; i++) {
    const rand = Math.floor(Math.random() * 10 + 1);
    arr.push({
      name: rand > 5 ? `Compra en falabella.com` : `Compra en otros comercios`,
      points: rand,
      date: "2020-09-26T00:00:00",
    });
  }
  return arr;
};

const events = genDummyEvents(100);

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: "#FFF",
    height: "auto",
    padding: "16px 16px",
  },
  imgContainer: {
    marginRight: "16px",
  },
  header: {
    backgroundColor: "#fff",
    display: "flex",
    padding: "16px 16px",
    alignItems: "center",
    cursor: "pointer",
    margin: "10px 0",
  },
  quizTitle: {
    textAlign: "center",
    margin: "25px 0",
  },
  notificationText: {
    margin: "2px 0 0 10px",
  },
  notification: {
    display: "flex",
    fontSize: "16px",
    margin: "20px 10px 35px 0",
    padding: "15px 20px",
    backgroundColor: "#eeeeee8f",
  },
  link: {
    textDecoration: "none",
    textTransform: "capitalize",
    fontWeight: "bold",
  },
}));

function QuizPage() {
  const { setUserProducts } = useUserProducts();
  const classes = useStyles();
  const { questions, quizTitle, answers } = dummyData;
  const [currentIndex, setCurrentIndex] = useState(0);
  const [results, setResults] = useState([]);
  const { grantAchievement } = useUserAchievements();

  const selectOption = (index, option = undefined) => {
    setCurrentIndex(index);

    if (option) {
      const selectedOption = {
        question: currentIndex,
        answerId: option.answerId,
      };
      const items = [...results];
      items[currentIndex] = selectedOption;
      setResults(items);
    }
    if (index >= questions.length) {
      grantAchievement(3);
      setUserProducts(userSelection.products);
    }
  };

  const userSelectionList = results
    .map((x) => x.answerId)
    .filter((v, i, a) => a.indexOf(v) !== i);

  const [userSelection] =
    userSelectionList.length > 0
      ? answers.filter((x) => x.id === userSelectionList[0])
      : answers;

  const isQuizCompleted = currentIndex === questions.length;

  return (
    <Container>
      <UserInfo events={events} />
      <div className={classes.header}>
        <img
          src="https://canjeonline-cmrpuntos.puntospoint.com/static/media/cmr-puntos.47a48ef8.png"
          width={50}
          height={50}
          className={classes.imgContainer}
        />
        <Typography variant="h5"> Responde y gana!</Typography>
      </div>

      <div className={classes.container}>
        <div className={classes.notification}>
          <SentimentVerySatisfiedIcon color="primary" />
          <span className={classes.notificationText}>
            Conocerte mejor nos ayuda a brindarte una mejor experiencia.
          </span>
          <span>
            <Link to="/" className={classes.link}>
              <Button size="small" color="primary">
                Volver a mi Dashboard
              </Button>
            </Link>
          </span>
        </div>
        <Divider />

        {!isQuizCompleted && (
          <Typography variant="h5" className={classes.quizTitle}>
            {quizTitle}
          </Typography>
        )}

        {!isQuizCompleted &&
          questions.map((question, i) => (
            <Questions
              {...question}
              onSelectOption={selectOption}
              isSelected={currentIndex === i}
              currentIndex={currentIndex}
            />
          ))}

        {isQuizCompleted && (
          <Result
            optionDescription={userSelection.optionDescription}
            option={userSelection.option}
            products={userSelection.products}
            showMoreText={userSelection.showMoreText}
            showMoreLink={userSelection.showMore}
          />
        )}
      </div>
    </Container>
  );
}

export default QuizPage;
