import React, { useState } from "react";
import { Container, makeStyles } from "@material-ui/core";
import UserInfo from "../../components/UserInfo/UserInfo";
import UserBenefits from "../../components/UserBenefits/UserBenefits";
import Notification from "../../components/Notification/Notification";
import SuggestedProducts from "../../components/SuggestedProducts/SuggestedProducts";
import CardGameModal from "../../components/CardGame/CardGameModal";
import useUserBenefits from "../../hooks/useUserBenefits";

const genDummyEvents = (number) => {
  let arr = [];
  for (let i = 0; i < number; i++) {
    const rand = Math.floor(Math.random() * 10 + 1);
    arr.push({
      name: rand > 5 ? `Compra en falabella.com` : `Compra en otros comercios`,
      points: rand,
      date: "2020-09-26T00:00:00",
    });
  }
  return arr;
};

const events = genDummyEvents(100);

const useStyles = makeStyles((theme) => ({
  gameNotification: {
    position: "absolute",
    bottom: "50%",
    right: "60px",
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const [isCardGameOpen, setCardGameOpen] = useState(false);
  const { addBenefit, userBenefits } = useUserBenefits();

  const handleGameClick = () => {
    setCardGameOpen(true);
  };
  const handleCardGameClose = () => {
    setCardGameOpen(false);
  };

  return (
    <>
      {isCardGameOpen && (
        <CardGameModal
          open={isCardGameOpen}
          onClose={handleCardGameClose}
          addBenefit={addBenefit}
        />
      )}
      <div>
        <Notification color="primary" text="Responde y gana!" url="/quiz" />
        <div className={classes.gameNotification}>
          <Notification
            color="primary"
            text=""
            url="/"
            type="GAME"
            onClick={handleGameClick}
          />
        </div>
        <Container>
          <UserInfo events={events} />
          <UserBenefits userBenefits={userBenefits} />
          <SuggestedProducts />
        </Container>
      </div>
    </>
  );
};

export default Dashboard;
