import useLocalStorage from "./useLocalStorage";
import { useEffect } from "react";

const initialProducts = [
  {
    image:
      "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/284/original/Calugas_Experiencias_Food_brunella-_4000.jpg",
    text: "4.000 pesos en Brunella",
    cmrPoints: "1000",
    url:
      "https://canjeonline-cmrpuntos.puntospoint.com/experiences/7fea637fd6d02b8f0adf6f7dc36aed93",
    expireDate: "31/12/2020",
  },
  {
    image:
      "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/002/189/original/Calugas_Experiencias_Food_tequila-_12000.jpg",
    text: "12.000 pesos en T Quila",
    cmrPoints: "3000",
    url:
      "https://canjeonline-cmrpuntos.puntospoint.com/experiences/757f843a169cc678064d9530d12a1881",
    expireDate: "31/12/2020",
  },
  {
    image:
      "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/004/083/original/8.000.jpg",
    text: "$8.000 pesos en Viajes Falabella",
    cmrPoints: "5000",
    url:
      "https://canjeonline-cmrpuntos.puntospoint.com/experiences/c91e3483cf4f90057d02aa492d2b25b1",
    expireDate: "19/08/2021",
  },
  {
    image:
      "https://s3.amazonaws.com/puntospoint-media/experiences/images/000/003/942/original/caluga_-POP_GRANDE_BEB_MED.png",
    text: "1 POP Grande y 2 bebidas",
    cmrPoints: "5000",
    url:
      "https://canjeonline-cmrpuntos.puntospoint.com/experiences/f44ee263952e65b3610b8ba51229d1f9",
    expireDate: "30/06/2021",
  },
];

const defaultProducts = initialProducts;
const storageKey = "products";

function useUserProducts() {
  const [userProducts, setUserProducts] = useLocalStorage(
    storageKey,
    defaultProducts
  );

  useEffect(() => {
    if (!window.localStorage.getItem(storageKey)) {
      setUserProducts(defaultProducts);
    }
  });

  return {
    userProducts,
    setUserProducts,
  };
}

export default useUserProducts;
