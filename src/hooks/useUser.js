import useLocalStorage from "./useLocalStorage";
import { useCallback, useEffect } from "react";

const userData = {
  name: "María",
  lastName: "De Las Mercedes",
  totalPuntos: 1500,
  cmrPuntos: {
    puntosDisponibles: "5500",
    puntosPorVencer: "1500",
    vencimiento: "2020-10-14",
  },
  level: 1,
  toNextLevel: 3500,
};

const defaultUser = userData;
const storageKey = "user";

function useUser() {
  const [user, setUser] = useLocalStorage(storageKey, defaultUser);

  useEffect(() => {
    if (!window.localStorage.getItem(storageKey)) {
      setUser(defaultUser);
    }
  });

  const subtractCmrPuntos = useCallback(
    (subtractAmount) => {
      setUser({
        ...user,
        cmrPuntos: {
          ...user.cmrPuntos,
          puntosDisponibles: user.cmrPuntos.puntosDisponibles - subtractAmount,
        },
      });
    },
    [user, setUser]
  );

  return {
    user,
    setUser,
    subtractCmrPuntos,
  };
}

export default useUser;
