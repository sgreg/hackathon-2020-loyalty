import useLocalStorage from "./useLocalStorage";
import { useCallback, useEffect } from "react";

const achievements = [
  {
    id: 1,
    type: "cmr",
    name: "¡Me encantan los descuentos!",
    description:
      "Vincula tu tarjeta CMR en Mi cuenta para poder usarla en futuras compras",
    points: 1,
  },
  {
    id: 2,
    type: "fpay",
    name: "Soy digital",
    description: "Haz una compra a través de Fpay",
    points: 1,
  },
  {
    id: 3,
    type: "promotions",
    name: "Questionario",
    description: "Completa tu primer questionario",
    points: 1,
  },
  {
    id: 4,
    type: "bag",
    name: "Tengo mucho estilo",
    description: "Haz una compra de vestuario",
    points: 1,
  },
];

const achievedByUser = [
  {
    id: 4,
    type: "bag",
    name: "Tengo mucho estilo",
    description: "Haz una compra de vestuario",
    points: 1,
  },
];

const defaultAchievements = achievements;
const defaultUserAchievments = achievedByUser;

const storageKey = "achievements";
const userStorageKey = "userAchievements";

function useAchievements() {
  const [achievements, setAchievements] = useLocalStorage(
    storageKey,
    defaultAchievements
  );
  const [userAchievements, setUserAchievements] = useLocalStorage(
    userStorageKey,
    defaultUserAchievments
  );

  useEffect(() => {
    if (!window.localStorage.getItem(storageKey)) {
      setAchievements(defaultAchievements);
    }
    if (!window.localStorage.getItem(userStorageKey)) {
      setUserAchievements(defaultUserAchievments);
    }
  });

  const grantAchievement = useCallback(
    (id) => {
      const achievement = achievements.find((x) => x.id === id);
      if (!achievement) {
        // unknown achievement
        return;
      }
      if (userAchievements.some((x) => x.id === id)) {
        // already granted
        return;
      }

      setUserAchievements([...userAchievements, achievement]);
    },
    [achievements, setUserAchievements, userAchievements]
  );

  return {
    achievements,
    userAchievements,
    setUserAchievements,
    grantAchievement,
  };
}

export default useAchievements;
