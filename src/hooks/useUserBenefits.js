import useLocalStorage from "./useLocalStorage";
import { useCallback, useEffect } from "react";

const fanBenefits = [
  {
    type: "giftcard",
    name: "Canje de productos, experiencias o giftcards",
  },
  {
    type: "promotions",
    name: "Promociones especiales",
  },
  {
    type: "time",
    name: "CMR puntos con 1 año de duración",
  },
  {
    type: "gift",
    name: "Descuentos exclusivos en tu cumpleaños",
  },
  {
    type: "info",
    name: "Doble acumulación en tu aniversario de participación de CMR puntos",
  },
  {
    type: "discount",
    name: "Dcto. en la administración de tu tarjeta CMR Falabella",
  },
  {
    type: "delivery",
    name: "3 despachos gratis al mes gratis comprando en falabella.com",
  },
  {
    type: "free",
    name: "Costo $0 en la mantención de tu Cuenta Corriente Banco Falabella",
  },
];

const defaultBenefits = fanBenefits;
const storageKey = "benefits";

function useUserBenefits() {
  const [userBenefits, setUserBenefits] = useLocalStorage(
    storageKey,
    defaultBenefits
  );

  useEffect(() => {
    if (!window.localStorage.getItem(storageKey)) {
      setUserBenefits(defaultBenefits);
    }
  });

  const addBenefit = useCallback(
    (benefit) => {
      setUserBenefits([...userBenefits, benefit]);
    },
    [setUserBenefits, userBenefits]
  );

  return {
    userBenefits,
    setUserBenefits,
    addBenefit,
  };
}

export default useUserBenefits;
