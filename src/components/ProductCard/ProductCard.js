import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 235,
    margin: "10px 20px",
    backgroundColor: "#fff",
    paddingBottom: "10px",

    [theme.breakpoints.down("sm")]: {
      maxWidth: 300,
      margin: "20px 20px",
      width: 300,
    },
  },
  media: {
    height: 140,
  },
  prices: {
    paddingTop: "10px",
    fontWeight: "bold",
    textAlign: "center",
    color: "#3AAD2B",
  },
  actions: {
    justifyContent: "center",
  },
  text: {
    margin: "5px 0 10px 0",
  },
}));

const ProductCard = ({
  image,
  text,
  cmrPoints,
  url,
  onRedeem = () => undefined,
}) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          component="img"
          className={classes.media}
          height="140"
          image={image}
          title="Product image"
        />
        <CardContent>
          <Typography
            variant="body2"
            color="textSecondary"
            component="p"
            className={classes.text}
          >
            {text}
          </Typography>
          <Typography
            className={classes.prices}
            gutterBottom
            color="primary"
            variant="h5"
            component="h2"
          >
            {cmrPoints} Puntos
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions className={classes.actions}>
        <Button
          size="small"
          variant="contained"
          color="primary"
          onClick={onRedeem}
        >
          Lo quiero!
        </Button>
        <Button size="small" color="primary" href={url} target="_blank">
          Ver más
        </Button>
      </CardActions>
    </Card>
  );
};

export default ProductCard;
