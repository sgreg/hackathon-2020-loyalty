import { Grid, makeStyles } from "@material-ui/core";
import React from "react";
import useUserAchievements from "../../hooks/useUserAchievements";
import DisplayItem from "../DisplayItem/DisplayItem";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 16,
  },
  content: {
    padding: 16,
    backgroundColor: "#fff",
    marginTop: 8,
  },
  gridItem: {
    flexGrow: 1,
  },
}));

const UserAchievements = () => {
  const { achievements, userAchievements } = useUserAchievements();

  const resolvedAchievements = achievements.map((a) => {
    return {
      ...a,
      disabled: !userAchievements.some((ua) => ua.id === a.id),
    };
  });

  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <Grid container spacing={2}>
          {resolvedAchievements.map((achievement, index) => {
            return (
              <Grid
                item
                sm={12}
                className={classes.gridItem}
                key={`user-achievement-item-${index}`}
              >
                <DisplayItem item={achievement} />
              </Grid>
            );
          })}
        </Grid>
      </div>
    </div>
  );
};

export default UserAchievements;
