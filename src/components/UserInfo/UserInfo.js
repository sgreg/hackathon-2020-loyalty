import React, { useEffect, useState } from "react";
import {
  CircularProgress,
  LinearProgress,
  makeStyles,
  Tooltip,
  Typography,
} from "@material-ui/core";
import UserRank from "../UserRank/UserRank";
import CustomModal from "../CustomModal/CustomModal";
import TimeLine from "../TimeLine/TimeLine";
import { Help } from "@material-ui/icons";
import { format } from "date-fns";
import useUser from "../../hooks/useUser";

import { ReactComponent as FanIcon } from "../../static/svg/FanIcon.svg";
import { ReactComponent as LoverIcon } from "../../static/svg/LoverIcon.svg";
import { ReactComponent as PremiumIcon } from "../../static/svg/PremiumIcon.svg";
import { ReactComponent as EliteIcon } from "../../static/svg/EliteIcon.svg";
import { ReactComponent as TrophyIcon } from "../../static/svg/trophy.svg";
import UserAchievements from "../UserAchievements/UserAchievements";

const levelIconSize = 24;

const useStyles = makeStyles((theme) => ({
  loaderContainer: {
    display: "flex",
    justifyContent: "center",
  },
  userInfoContainer: {
    position: "relative",
    display: "flex",
    backgroundColor: "#FFF",
    padding: "8px 16px",
    [theme.breakpoints.down("md")]: {
      flexDirection: "column",
    },
  },
  userAvatar: {
    width: 60,
    height: 60,
    backgroundImage:
      'url("https://www.falabella.com/a/fa/myaccount/static/images/defaultProfilePicture.svg")',
    backgroundPosition: "-3px top",
    objectFit: "cover",
    backgroundColor: "#f0f0f0",
    border: `2px solid ${theme.palette.primary.main}`,
    borderRadius: "50%",
    [theme.breakpoints.down("md")]: {
      margin: "auto",
    },
  },
  userDetails: {
    display: "flex",
    flexGrow: "1",
    flexDirection: "column",
    marginLeft: 8,
    [theme.breakpoints.down("md")]: {
      textAlign: "center",
    },
  },
  userNameLabel: {
    fontWeight: "bold",
  },
  levelLabel: {
    marginRight: 4,
  },
  nextLevelLabel: {
    marginLeft: 4,
    color: theme.palette.text.disabled,
  },
  progressContainer: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      alignItems: "center",
      justifyContent: "center",
    },
  },
  progresBarContainer: {
    width: 200,
    paddingTop: 10,
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
      alignSelf: "center",
      width: 150,
    },
    [theme.breakpoints.down("sm")]: {
      width: 130,
    },
  },
  cmrPuntosProgressText: {
    fontSize: 10,
  },
  rankBadgeContainer: {
    alignSelf: "flex-end",
    [theme.breakpoints.down("md")]: {
      position: "absolute",
      top: 8,
      right: 8,
    },
  },
  helpButton: {
    cursor: "pointer",
    color: theme.palette.primary.main,
    background: "none",
    border: "none",
    "&:focus": {
      outline: "none",
    },
    alignSelf: "end",
  },
  cmrBalanceContainer: {
    marginRight: 8,
    alignSelf: "center",
    [theme.breakpoints.down("md")]: {
      marginTop: 8,
      marginBottom: 8,
    },
  },
  balanceValue: {
    fontWeight: 200,
  },
  balanceText: {
    display: "flex",
    [theme.breakpoints.down("md")]: {
      justifyContent: "center",
    },
  },
  balanceBigText: {
    fontSize: 24,
    [theme.breakpoints.down("md")]: {
      fontSize: 22,
    },
  },
  smallText: {
    fontSize: 12,
  },
  currentLevelContainer: {
    marginRight: 8,
  },
  nextLevelContainer: {
    marginLeft: 8,
  },
  disabledLevel: {
    opacity: 0.4,
  },
  achievmentsContainer: {
    cursor: "pointer",
    display: "flex",
    alignSelf: "center",
    flexDirection: "column",
    marginLeft: 8,
    alignItems: "center",
    "&:hover": {
      "& svg": {
        fill: theme.palette.primary.dark,
      },
    },
    [theme.breakpoints.down("md")]: {
      position: "absolute",
      top: 8,
      left: 8,
    },
  },
  trophyIcon: {
    fill: theme.palette.primary.main,
    marginTop: 2,
    marginBottom: 2,
  },
  achievmentsText: {
    fontWeight: "bold",
  },
}));

const UserInfo = ({ events }) => {
  const classes = useStyles();

  const [loading, setLoading] = useState(true);
  const [showTimeline, setShowTimeline] = useState(false);
  const [showAchievements, setshowAchievements] = useState(false);
  const { user } = useUser();

  useEffect(() => {
    setLoading(false);
  }, []);

  const totalPoints = user.totalPuntos + user.toNextLevel;

  const progress = (user.totalPuntos / totalPoints) * 100;

  const toggleTimeLineModal = () => {
    setShowTimeline(!showTimeline);
  };

  const toggleAchievementsModal = () => {
    setshowAchievements(!showAchievements);
  };

  const solveLevelData = (total) => {
    if (total < 5000) {
      return {
        currentName: "Fan",
        nextName: "Lover",
        current: <FanIcon width={levelIconSize} height={levelIconSize} />,
        next: <LoverIcon width={levelIconSize} height={levelIconSize} />,
      };
    } else if (total >= 5000 && total < 15000) {
      return {
        currentName: "Lover",
        nextName: "Premium",
        current: <LoverIcon width={levelIconSize} height={levelIconSize} />,
        next: <PremiumIcon width={levelIconSize} height={levelIconSize} />,
      };
    } else if (total >= 15000 && total < 50000) {
      return {
        currentName: "Premium",
        nextName: "Elite",
        current: <PremiumIcon width={levelIconSize} height={levelIconSize} />,
        next: <EliteIcon width={levelIconSize} height={levelIconSize} />,
      };
    } else {
      return {
        currentName: "Elite",
        nextName: "Elite",
        current: <PremiumIcon width={levelIconSize} height={levelIconSize} />,
        next: <EliteIcon width={levelIconSize} height={levelIconSize} />,
      };
    }
  };

  const levelData = solveLevelData(user.totalPuntos);

  return loading ? (
    <div className={classes.loaderContainer}>
      <CircularProgress />
    </div>
  ) : (
    <>
      <div className={classes.userInfoContainer}>
        <div className={classes.userAvatar} />
        <div className={classes.userDetails}>
          <span>
            <Typography>Hola, </Typography>
            <Typography className={classes.userNameLabel}>
              {user.name} {user.lastName}
            </Typography>
          </span>
          <div className={classes.progressContainer}>
            <Tooltip title={levelData.currentName} placement="top">
              <div className={classes.currentLevelContainer}>
                {levelData.current}
              </div>
            </Tooltip>
            <Tooltip
              title="Puntos CMR acumulados históricamente"
              placement="top"
            >
              <div className={classes.progresBarContainer}>
                <LinearProgress variant="determinate" value={progress} />
                <span>
                  <Typography className={classes.cmrPuntosProgressText}>
                    {user.totalPuntos}/{totalPoints} CMR puntos
                  </Typography>
                </span>
              </div>
            </Tooltip>
            <Tooltip title={levelData.nextName} placement="top">
              <div
                className={`${classes.nextLevelContainer} ${
                  levelData.currentName !== "Elite" ? classes.disabledLevel : ""
                }`}
              >
                {levelData.next}
              </div>
            </Tooltip>
            <button
              onClick={toggleTimeLineModal}
              className={classes.helpButton}
            >
              <Help />
            </button>
          </div>
        </div>
        <div className={classes.cmrBalanceContainer}>
          <span className={classes.balanceText}>
            <Typography variant="h5" className={classes.balanceBigText}>
              Saldo CMR puntos:&nbsp;
            </Typography>
            <Typography
              variant="h5"
              className={`${classes.balanceBigText} ${classes.balanceValue}`}
            >
              {user.cmrPuntos.puntosDisponibles}
            </Typography>
          </span>
          <span className={classes.balanceText}>
            <Typography className={classes.smallText}>
              Puntos por vencer al{" "}
              {format(new Date(user.cmrPuntos.vencimiento), "dd/MM/yyyy")}
              :&nbsp;
            </Typography>
            <Typography
              className={`${classes.balanceValue} ${classes.smallText}`}
            >
              {user.cmrPuntos.puntosPorVencer}
            </Typography>
          </span>
        </div>
        <div className={classes.rankBadgeContainer}>
          <UserRank cmrPuntos={user.totalPuntos} />
        </div>
        <div
          className={classes.achievmentsContainer}
          onClick={toggleAchievementsModal}
        >
          <TrophyIcon width="36" height="36" className={classes.trophyIcon} />
          <Typography className={classes.achievmentsText}>Logros</Typography>
        </div>
      </div>
      <CustomModal
        open={showTimeline}
        handleClose={toggleTimeLineModal}
        title={"Mi historial de puntos"}
      >
        <TimeLine events={events} />
      </CustomModal>
      <CustomModal
        open={showAchievements}
        handleClose={toggleAchievementsModal}
        title={"Mis logros"}
      >
        <UserAchievements />
      </CustomModal>
    </>
  );
};

export default UserInfo;
