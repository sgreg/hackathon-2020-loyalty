import { makeStyles, Typography } from "@material-ui/core";
import React from "react";
import useUserProducts from "../../hooks/useUserProducts";
import ProductCard from "../ProductCard/ProductCard";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 16,
  },
  header: {
    backgroundColor: "#fff",
    display: "flex",
    padding: "16px 16px",
    alignItems: "center",
  },
  cardContainer: {
    display: "flex",
    justifyContent: "center",
    padding: 16,
    backgroundColor: "white",
    marginTop: 8,
    marginBottom: 16,
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
}));

const SuggestedProducts = () => {
  const classes = useStyles();

  const { userProducts: products } = useUserProducts();

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <Typography variant="h5">Promociones</Typography>
      </div>
      <div className={classes.cardContainer}>
        {products.slice(0, 4).map((x) => (
          <ProductCard
            image={x.image}
            text={x.text}
            cmrPoints={x.cmrPoints}
            url={x.url}
          />
        ))}
      </div>
    </div>
  );
};

export default SuggestedProducts;
