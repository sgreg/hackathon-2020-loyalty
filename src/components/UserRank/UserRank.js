import { makeStyles, Typography } from "@material-ui/core";
import React from "react";

import { ReactComponent as FanIcon } from "../../static/svg/FanIcon.svg";
import { ReactComponent as LoverIcon } from "../../static/svg/LoverIcon.svg";
import { ReactComponent as PremiumIcon } from "../../static/svg/PremiumIcon.svg";
import { ReactComponent as EliteIcon } from "../../static/svg/EliteIcon.svg";

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
  },
  iconStyle: {
    margin: "auto",
  },
  textStyle: {
    fontWeight: "bold",
  },
}));

const UserRank = ({ cmrPuntos }) => {
  const classes = useStyles();
  if (cmrPuntos < 5000) {
    return (
      <div className={classes.root}>
        <FanIcon width="40" height="40" className={classes.iconStyle} />
        <Typography className={classes.textStyle}>Fan</Typography>
      </div>
    );
  }
  if (cmrPuntos >= 5000 && cmrPuntos < 15000) {
    return (
      <div className={classes.root}>
        <LoverIcon width="40" height="40" className={classes.iconStyle} />
        <Typography className={classes.textStyle}>Lover</Typography>
      </div>
    );
  }
  if (cmrPuntos >= 15000 && cmrPuntos < 50000) {
    return (
      <div className={classes.root}>
        <PremiumIcon width="40" height="40" className={classes.iconStyle} />
        <Typography className={classes.textStyle}>Premium</Typography>
      </div>
    );
  }
  if (cmrPuntos >= 50000) {
    return (
      <div className={classes.root}>
        <EliteIcon width="40" height="40" className={classes.iconStyle} />
        <Typography className={classes.textStyle}>Elite</Typography>
      </div>
    );
  }
};

export default UserRank;
