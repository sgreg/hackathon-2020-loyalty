import { makeStyles, Modal, Typography } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import classNames from "classnames";

const useStyles = makeStyles((theme) => ({
  modalContainer: {
    backgroundColor: theme.palette.background.white,
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 600,
    maxHeight: 600,
    overflow: "auto",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      maxHeight: "100%",
      top: 0,
      left: 0,
      transform: "unset",
    },
    "&:focus": {
      outline: "none",
    },
  },
  modalHeader: {
    backgroundColor: theme.palette.background.white,
    position: "sticky",
    top: 0,
    display: "flex",
    padding: 16,
    borderBottom: `1px solid ${theme.palette.text.disabled}`,
  },
  modalTitle: {
    flexGrow: 1,
  },
  closeButton: {
    cursor: "pointer",
    padding: 0,
    margin: 0,
    border: 0,
    background: "none",
    "&:focus": {
      outline: "none",
    },
  },
}));

const CustomModal = ({
  children,
  open,
  handleClose,
  title,
  hideCloseButton,
  className,
}) => {
  const classes = useStyles();

  return (
    <Modal open={open} onClose={handleClose}>
      <div className={classNames(classes.modalContainer, className)}>
        {title && (
          <div className={classes.modalHeader}>
            <Typography className={classes.modalTitle} variant="h5">
              {title}
            </Typography>
            {!hideCloseButton && (
              <button className={classes.closeButton} onClick={handleClose}>
                <CloseIcon />
              </button>
            )}
          </div>
        )}
        {children}
      </div>
    </Modal>
  );
};

export default CustomModal;
