import { makeStyles, Popover, Typography } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { format } from "date-fns";

const elementSize = 64;

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    padding: 20,
  },
  timeLineItem: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: elementSize,
    height: elementSize,
    borderRadius: 6,
  },
  row: {
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 2,
    "&.last": {
      justifyContent: "flex-start",
    },
  },
  evenRow: {
    flexDirection: "row",
  },
  oddRow: {
    flexDirection: "row-reverse",
  },
  evenItem: {
    backgroundColor: theme.palette.primary.main,
    "& .text": {
      color: theme.palette.primary.contrastText,
    },
  },
  oddItem: {
    backgroundColor: theme.palette.background.paper,
    "& .text": {
      color: theme.palette.text.default,
    },
  },
  eventLabel: {
    fontWeight: "bold",
  },
  timeLineInfo: {
    fontSize: 16,
    fontWeight: 200,
  },
  timelineContainer: {
    marginTop: 8,
  },
  pointsItem: {
    cursor: "pointer",
  },
  popOverContent: {
    display: "flex",
    flexDirection: "column",
    padding: 16,
    textAlign: "center",
  },
}));

const TimeLine = ({ events }) => {
  const classes = useStyles();
  const ref = useRef(null);

  const [elementsPerEvenRow, setElementPerEvenRow] = useState();
  const [elementsMap, setElementsMap] = useState([]);

  const [anchorEl, setAnchorEl] = useState();
  const [popOverItem, setPopOverItem] = useState();

  const handleClick = (event, item) => {
    setAnchorEl(event.currentTarget);
    setPopOverItem(item);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    if (ref && ref.current) {
      setElementPerEvenRow(
        Math.floor(ref.current.getBoundingClientRect().width / elementSize)
      );
    }
  }, [ref]);

  useEffect(() => {
    if (elementsPerEvenRow) {
      let iterator = elementsPerEvenRow;
      let eventsMap = [];
      let eventsRow = [];
      const newEvents = events.reduce((r, a) => r.concat(a, 0), [{}]);
      newEvents.shift();
      newEvents.pop();
      newEvents.forEach((event, eventIndex) => {
        const newEvent = {
          ...event,
          even: eventIndex % 2 === 0,
        };
        if (iterator === -1) {
          eventsMap.push([newEvent]);
          iterator = elementsPerEvenRow;
        } else {
          eventsRow.push(newEvent);
          iterator--;
          if (iterator === 0 || eventIndex === events.length - 1) {
            eventsMap.push(eventsRow);
            eventsRow = [];
            iterator = -1;
          }
        }
      });
      setElementsMap(eventsMap);
    }
  }, [elementsPerEvenRow, events]);

  let evenOrOdd = true;
  let count = 0;

  const solveEvenRow = (elementIndex) => {
    if (elementIndex === 0) {
      return true;
    } else {
      if (count === 0) {
        evenOrOdd = !evenOrOdd;
        count = 1;
        return evenOrOdd;
      } else {
        count--;
        return evenOrOdd;
      }
    }
  };

  const openPopOver = Boolean(anchorEl);

  return (
    <div className={classes.root} ref={ref}>
      <Typography className={classes.timeLineInfo}>
        Estos son los eventos que te dieron puntos del mas reciente al más
        antiguo
      </Typography>
      <div className={classes.timelineContainer}>
        {elementsMap.map((element, elementIndex) => {
          const isEven = solveEvenRow(elementIndex);
          return (
            <div
              className={`${
                elementIndex === elementsMap.length - 1 ? "last" : ""
              } ${classes.row} ${isEven ? classes.evenRow : classes.oddRow}`}
              key={`timeline-row-${elementIndex}`}
            >
              {element.map((item, itemIndex) => {
                return (
                  <div
                    className={`${classes.timeLineItem} ${
                      item.points ? classes.pointsItem : ""
                    } ${item.even ? classes.evenItem : classes.oddItem}`}
                    key={`timeline-item-${itemIndex}`}
                    onClick={
                      item.points
                        ? (evt) => {
                            handleClick(evt, item);
                          }
                        : undefined
                    }
                  >
                    <Typography className={`text ${classes.eventLabel}`}>
                      {item.points ? `+${item.points}` : " "}
                    </Typography>
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
      <Popover
        open={openPopOver}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
      >
        {popOverItem && (
          <div className={classes.popOverContent}>
            <Typography>{popOverItem.name}</Typography>
            <Typography>
              {format(new Date(popOverItem.date), "dd/MM/yyyy HH:mm")}
            </Typography>
          </div>
        )}
      </Popover>
    </div>
  );
};

export default TimeLine;
