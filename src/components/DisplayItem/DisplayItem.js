import { makeStyles, Typography } from "@material-ui/core";
import React from "react";

import { ReactComponent as FpayLogo } from "../../static/svg/FpayLogo.svg";
import { ReactComponent as FpayLogoDisabled } from "../../static/svg/FpayLogoDisabled.svg";

const useStyles = makeStyles((theme) => ({
  displayItemContainer: {
    display: "flex",
    alignItems: "center",
    padding: 16,
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: 8,
    "&.disabled": {
      borderColor: theme.palette.text.disabled,
    },
  },
  icon: {
    fontSize: 32,
    minWidth: 57,
    color: theme.palette.primary.main,
    textAlign: "center",
    "&.disabled": {
      color: theme.palette.text.disabled,
    },
  },
  displayItemLabelContainer: {
    marginLeft: 8,
    display: "flex",
    flexDirection: "column",
  },
  displayItemName: {
    fontSize: 16,
    fontWeight: "bold",
  },
  displayItemDescription: {
    fontSize: 14,
    fontStyle: "italic",
    color: theme.palette.text.hint,
  },
  disabledText: {
    color: theme.palette.text.disabled,
  },
}));

const solveDisplayIcon = (itemType, className, disabled) => {
  switch (itemType) {
    case "discount": {
      return <i className={`csicon-Cpupon ${className}`}></i>;
    }
    case "bag": {
      return <i className={`csicon-bag ${className}`}></i>;
    }
    case "cmr": {
      return <i className={`csicon-CMR ${className}`}></i>;
    }
    case "fpay": {
      const svgProps = {
        width: "65",
        height: "32",
        viewBox: "0 0 180 100",
        preserveAspectRatio: "xMidYMid meet",
      };
      return disabled ? (
        <FpayLogoDisabled {...svgProps} />
      ) : (
        <FpayLogo {...svgProps} />
      );
    }
    case "deliveryDiscount": {
      return <i className={`csicon-Orders ${className}`}></i>;
    }
    case "delivery": {
      return <i className={`csicon-delivery ${className}`}></i>;
    }
    case "gift":
    case "specialDiscount": {
      return <i className={`csicon-Giftcard ${className}`}></i>;
    }
    case "giftcard": {
      return <i className={`csicon-new_giftcard ${className}`}></i>;
    }
    case "time": {
      return <i className={`csicon-history ${className}`}></i>;
    }
    case "free": {
      return (
        <span className={`${className}`} style={{ fontSize: 22 }}>
          $0
        </span>
      );
    }
    case "promotions": {
      return <i className={`csicon-star_full_filled ${className}`}></i>;
    }
    default: {
      return <i className={`csicon-info ${className}`}></i>;
    }
  }
};

const DisplayItem = ({ item }) => {
  const classes = useStyles();
  return (
    <div
      className={`${classes.displayItemContainer} ${
        item.disabled ? "disabled" : ""
      }`}
    >
      {solveDisplayIcon(
        item.type,
        `${classes.icon} ${item.disabled ? "disabled" : ""}`,
        item.disabled
      )}
      <div className={classes.displayItemLabelContainer}>
        <Typography
          className={`${classes.displayItemName} ${
            item.disabled ? classes.disabledText : ""
          }`}
        >
          {item.name}
        </Typography>
        {item.description && (
          <Typography className={classes.displayItemDescription}>
            {item.description}
          </Typography>
        )}
      </div>
    </div>
  );
};

export default DisplayItem;
