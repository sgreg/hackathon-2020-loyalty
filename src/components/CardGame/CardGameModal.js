import React, { useState } from "react";

import CustomModal from "../CustomModal/CustomModal";
import CardGame from "./CardGame";
import { makeStyles, Typography } from "@material-ui/core";
import useUser from "../../hooks/useUser";
import CardGiftcardOutlinedIcon from "@material-ui/icons/CardGiftcardOutlined";
import { useTransition, animated } from "react-spring";

const useStyles = makeStyles((theme) => ({
  modal: {
    width: 1300,
    maxHeight: 800,
  },
  userNameLabel: {
    fontWeight: "bold",
  },
  instructions: {
    padding: "1rem",
    width: 800,
  },
  icon: {
    marginRight: "10px",
  },
  giftTextContainer: {
    height: 120,
  },
  giftText: {
    margin: "20px 10px 30px 10px",
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  couponCodeText: {
    textAlign: "center",
    paddingBottom: "30px",
  },
}));

function CardGameModal({ open, onClose, addBenefit }) {
  const classes = useStyles();
  const { user } = useUser();
  const [isRevealed, setRevealed] = useState(false);
  const transitions = useTransition(isRevealed, null, {
    from: {
      transform: "translate3d(0, -40px, 0)",
    },
    enter: {
      transform: "translate3d(0, 0px, 0)",
    },
  });

  const handleCardReveal = () => {
    setRevealed(true);
  };

  return (
    <CustomModal
      open={open}
      handleClose={onClose}
      title="Juega para ganar beneficios y descuentos!"
      className={classes.modal}
    >
      <div className={classes.instructions}>
        <Typography>Hola, </Typography>
        <Typography className={classes.userNameLabel}>
          {user.name} {user.lastName}!
        </Typography>
        <Typography>
          {" "}
          Selecciona una de las 3 cartas para ganar premios y descuentos!
        </Typography>
      </div>
      <CardGame onReveal={handleCardReveal} addBenefit={addBenefit} />
      <div className={classes.giftTextContainer}>
        {transitions.map(({ item, props }) =>
          item ? (
            <animated.div style={props}>
              <div className={classes.giftText}>
                <CardGiftcardOutlinedIcon
                  color="primary"
                  className={classes.icon}
                />
                <Typography variant="h5" component="h2">
                  Felicidades! puedes ver tu cupon en mis beneficios
                </Typography>
              </div>
              <Typography component="p" className={classes.giftText}>
                Código del cupon: ACD987GHJ / Expira en: 21/12/2020
              </Typography>
            </animated.div>
          ) : null
        )}
      </div>
    </CustomModal>
  );
}

export default CardGameModal;
