import React, { useState } from "react";
import { useSpring, animated } from "react-spring";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";

import "./card.css";
import { ReactComponent as CardBackside } from "./card-back.svg";

const AnimatedBackside = animated(CardBackside);

const useStyles = makeStyles((theme) => ({
  root: {
    position: "relative",
    "max-width": "300px",
    "max-height": "400px",
    width: "50ch",
    height: "50ch",
  },
}));

function Card(props) {
  const { onClick, className, FrontCard } = props;
  const classes = useStyles(props);
  const [flipped, setFlipped] = useState(false);
  const { transform, opacity } = useSpring({
    opacity: flipped ? 1 : 0,
    transform: `perspective(600px) rotateY(${flipped ? 180 : 0}deg)`,
    config: { mass: 5, tension: 500, friction: 80 },
  });

  const handleClick = () => {
    if (onClick) {
      const allowClicking = onClick();
      if (allowClicking === false) {
        return;
      }
    }
    setFlipped((state) => !state);
  };

  return (
    <div onClick={handleClick} className={classes.root}>
      <AnimatedBackside
        className={classNames("back", className)}
        height={400}
        width={300}
        style={{
          opacity: opacity.interpolate((o) => 1 - o),
          transform,
        }}
      />
      <FrontCard
        className={classNames("front", className)}
        height={400}
        width={300}
        style={{
          opacity,
          transform: transform.interpolate((t) => `${t} rotateY(180deg)`),
        }}
      />
    </div>
  );
}

export default Card;
