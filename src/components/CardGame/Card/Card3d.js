import { animated, useSpring } from "react-spring";
import React from "react";
import { ReactComponent as CardBackside } from "./card-back.svg";

const AnimatedBackside = animated(CardBackside);

const calc = (x, y) => [
  -(y - window.innerHeight / 2) / 20,
  (x - window.innerWidth / 2) / 20,
  1.001,
];
const trans = (x, y, s) =>
  `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;

function Card3d({ style }) {
  const [props, set3d] = useSpring(() => ({
    xys: [0, 0, 1],
    config: { mass: 5, tension: 350, friction: 40 },
  }));

  const onMouseMove = ({ clientX: x, clientY: y }) =>
    set3d({ xys: calc(x, y) });
  const onMouseLeave = () => set3d({ xys: [0, 0, 1] });

  return (
    <AnimatedBackside
      className="front"
      height={400}
      width={300}
      onMouseMove={onMouseMove}
      onMouseLeave={onMouseLeave}
      style={{
        transform: props.xys.interpolate(trans),
        ...style,
      }}
    />
  );
}

export default Card3d;
