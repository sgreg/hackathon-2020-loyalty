import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import classNames from "classnames";
import Card from "./Card/Card";
import "./card-game.css";
import { animated } from "react-spring";
import { ReactComponent as CardFront } from "./Card/card-front-juan-valdez.svg";

const AnimatedFront = animated(CardFront);

function CardGame({ onReveal = () => undefined, addBenefit }) {
  const [cards] = useState(
    Array.from({ length: 3 }).map(() => ({
      front: AnimatedFront,
    }))
  );
  const [revealedCard, setRevealedCard] = useState();

  const handleCardSelection = (index) => () => {
    if (revealedCard !== undefined && revealedCard !== index) {
      return false;
    }

    if (revealedCard > -1) {
      return;
    }
    setRevealedCard(index);
    addBenefit({
      type: "info",
      name:
        "Descuento único 50% en café Juan Valdez | Cupón: ACD987GHJ - Expira: 21/12/2020",
    });
    onReveal();
  };

  return (
    <Grid item xs={12}>
      <Grid container justify="center" spacing={1}>
        {cards.map((card, index) => (
          <Grid key={index} item>
            <Card
              onClick={handleCardSelection(index)}
              className={classNames({
                "card-revealed": revealedCard === index,
                "card-disabled":
                  revealedCard !== undefined && revealedCard !== index,
              })}
              FrontCard={card.front}
            />
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
}

export default CardGame;
