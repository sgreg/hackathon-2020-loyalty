import { Container, makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

const headerFooterHeight = 60;
const contentMargin = 52;

const useStyles = makeStyles((theme) => ({
  headerFooterBar: {
    background: "white",
    height: 60,
  },
  headerFooterBarContainer: {
    position: "relative",
    display: "flex",
    justifyContent: "space-between",
  },
  content: {
    marginTop: contentMargin,
    minHeight: `calc(100% - ${headerFooterHeight * 2 + contentMargin}px)`,
  },
  falabellaLogo: {
    width: 64,
    height: 100,
    backgroundColor: theme.palette.primary.main,
    display: "",
  },
  falabellaLogoImage: {
    width: 64,
    height: 83,
    marginTop: 8,
  },
  helpTextContainer: {
    marginTop: 20,
  },
  helpText: {
    fontSize: 14,
  },
  boldText: {
    fontWeight: "bold",
  },
  footerTextContainer: {
    marginTop: 24,
  },
}));

const Layout = ({ children }) => {
  const classes = useStyles();

  return (
    <>
      <div className={classes.headerFooterBar}>
        <Container className={classes.headerFooterBarContainer}>
          <Link to="/">
            <div className={classes.falabellaLogo}>
              <img
                className={classes.falabellaLogoImage}
                alt="Falabella logo"
                src="data:image/svg+xml;base64,PCEtLSBHZW5lcmF0ZWQgYnkgSWNvTW9vbi5pbyAtLT4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjMzNiIgaGVpZ2h0PSI3NjgiIHZpZXdCb3g9IjAgMCAzMzYgNzY4Ij4KPHBhdGggZmlsbD0iI2ZmZiIgZD0iTTMwNy4yMjEgNDU0LjM3OHEtMTEuOTU3IDAtMjAuMjM0LTguNzM3dC04LjI3OC0yMC42OTQgOC4yNzgtMjAuNjk0IDIwLjIzNC04LjczNyAyMC4yMzQgOC43MzcgOC4yNzggMjAuNjk0LTguMjc4IDIwLjY5NC0yMC4yMzQgOC43Mzd6TTMzMy44OTMgMjMuMDI3bC05LjE5Ny0wLjkxOXQtMTUuNjM1IDAuOTE5cS05LjE5NyAxLjg0LTE1LjYzNSA1Ljk3OHQtMTIuODc2IDExLjQ5N3EtMTEuOTU3IDExLjk1Ni0xNy45MzUgMzEuNzN0LTEzLjMzNiA1Mi44ODRsLTM0Ljk0OSAxNzIuOTA4LTUuNTE4IDI1Ljc1MmgxMDQuODQ5bC00LjU5OCAxNC43MTZoLTEwMy4wMTBsLTYwLjcwMiAyODkuNzEzcS02LjQzOCAzMS4yNy0xNS4xNzYgNTcuOTQzdC0zMS43MyA0OC43NDVxLTE1LjYzNSAxNS42MzUtMzUuNDEgMjQuMzczdC00MC45MjggOC43MzhoLTEwLjExN2wxLjg0LTEwLjExN2g5LjE5N3ExMS45NTctMS44NCAyMC4yMzQtNi40Mzh0MTUuNjM1LTEzLjc5NXExMS4wMzctMTQuNzE2IDE3LjQ3NS0zMi4xOTF0MTMuNzk1LTUwLjU4NWw2Ni4yMi0zMTYuMzg1aC05NC43MzJsMy42NzktMTQuNzE2aDkzLjgxMmw2LjQzOC0yNy41OTIgMjguNTExLTE0NC4zOTdxNi40MzgtMzEuMjcxIDE1LjYzNS01Ny45NDN0MzEuMjctNDguNzQ1cTE1LjYzNS0xNS42MzUgMzUuNDEtMjQuMzczdDQwLjkyOC04LjczOGw2LjQzOCAwLjQ2dDExLjk1NyAwLjQ2bC0xLjg0IDEwLjExN3oiPjwvcGF0aD4KPC9zdmc+Cg=="
              />
            </div>
          </Link>
          <div className={classes.helpTextContainer}>
            <Typography className={classes.helpText}>
              ¿Necesitas ayuda?&nbsp;&nbsp;Llámanos al&nbsp;&nbsp;
            </Typography>
            <Typography className={`${classes.helpText} ${classes.boldText}`}>
              600 390 6500
            </Typography>
          </div>
        </Container>
      </div>
      <div className={classes.content}>{children}</div>
      <div className={classes.headerFooterBar}>
        <Container className={classes.headerFooterBarContainer}>
          <div className={classes.footerTextContainer}>
            <Typography className={classes.helpText}>
              © Todos los derechos reservados
            </Typography>
            &emsp;
            <Typography className={`${classes.helpText} ${classes.boldText}`}>
              Falabella Retail S. A. Manuel Rodrí­guez 730, Santiago de Chile.
            </Typography>
          </div>
        </Container>
      </div>
    </>
  );
};

export default Layout;
