import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ProductCard from "../ProductCard/ProductCard";
import CheckIcon from "@material-ui/icons/Check";
import { Container, Typography } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import useUser from "../../hooks/useUser";
import CustomModal from "../CustomModal/CustomModal";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    backgroundColor: "#fff",
  },
  media: {
    height: 140,
  },
  prices: {
    fontWeight: "bold",
    textAlign: "center",
  },
  cardContainer: {
    display: "flex",
    justifyContent: "center",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
  resultHeader: {
    margin: "20px 0 20px 10px",
  },
  optionTitle: {
    fontWeight: "bold",
    textAlign: "center",
  },
  optionDescriptionText: {
    textAlign: "center",
    margin: "15px 0",
  },
  headerMessageContainer: {
    display: "flex",
    alignItems: "center",
  },
  moreOptions: {
    margin: "30px 30px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  moreOptionsButton: {
    margin: "20px 20px",
    width: "auto",
  },
  modalContainer: {
    paddingBottom: "35px",
  },
  modalText: {
    margin: "20px 10px",
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  },
  buttonLink: {
    textDecoration: "none",
    margin: "20px 20px",
  },
  buttonContainer: {
    textAlign: "right",
    marginTop: "30px",
  },
}));

const Result = ({
  products,
  option,
  optionDescription,
  showMoreText,
  showMoreLink,
}) => {
  const classes = useStyles();
  const { subtractCmrPuntos } = useUser();
  const [isRedeemModalOpen, setRedeemModalOpen] = useState(false);

  const handleRedeem = (product) => () => {
    subtractCmrPuntos(Number(product.cmrPoints));
    setRedeemModalOpen(true);
  };
  const handleRedeemModalClose = () => {
    setRedeemModalOpen(false);
  };

  return (
    <>
      <CustomModal
        open={isRedeemModalOpen}
        handleClose={handleRedeemModalClose}
        title="Gracias Por Preferirnos María!"
        className={classes.modalContainer}
      >
        <div className={classes.modalText}>
          <CheckIcon color="primary" />
          <Typography variant="h5" component="h2">
            Canje finalizado exitosamente
          </Typography>
        </div>
        <div className={classes.buttonContainer}>
          <Link to="/" className={classes.buttonLink}>
            <Button size="small" variant="contained" color="primary">
              Volver a mi Dashboard
            </Button>
          </Link>
        </div>
      </CustomModal>
      <Container>
        <div className={classes.headerMessageContainer}>
          <CheckIcon color="primary" />
          <Typography
            className={classes.resultHeader}
            variant="h5"
            component="h2"
          >
            Gracias por responder nuestro Quiz!
          </Typography>
        </div>

        <Typography
          className={classes.optionTitle}
          variant="h5"
          component="h2"
          color="primary"
        >
          {option}
        </Typography>

        <Typography
          variant="subtitle1"
          gutterBottom
          className={classes.optionDescriptionText}
        >
          {optionDescription}
        </Typography>

        <div className={classes.cardContainer}>
          {products.map((x) => (
            <ProductCard
              image={x.image}
              text={x.text}
              cmrPoints={x.cmrPoints}
              url={x.url}
              onRedeem={handleRedeem(x)}
            />
          ))}
        </div>

        <div className={classes.moreOptions}>
          <Typography variant="h6" component="h6">
            Tenemos más opciones para ti
          </Typography>
          <Button
            className={classes.moreOptionsButton}
            size="small"
            variant="contained"
            color="primary"
            href={showMoreLink}
            target="_blank"
          >
            {showMoreText}
          </Button>
        </div>
      </Container>
    </>
  );
};

export default Result;
