import React, { useState } from "react";
import {
  Container,
  makeStyles,
  Grid,
  IconButton,
  Typography,
} from "@material-ui/core";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

import classNames from "classnames";

const useStyles = makeStyles((theme) => ({
  optionBox: {
    textAlign: "center",
    minHeight: "100px",
    margin: "10px 10px 10px",
    padding: "10px 10px 10px",
    backgroundColor: "#EEEEEE",
    color: "#333",
    fontSize: "16px",
    fontWeight: "bold",
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12)",
    cursor: "pointer",
  },
  selectedOptionBox: {
    backgroundColor: "#AAD503",
    color: "#FFF",
  },
  questionBox: {
    display: "flex",
    margin: "20px 0 40px 0",
  },
  question: {
    display: "flex",
    alignItems: "center",
  },
  questionText: {
    marginLeft: "10px",
  },
  optionsContainer: {
    display: "flex",
    flexDirection: "row",
    width: "90%",
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
      alignItems: "center",
    },
  },
}));

const Questions = ({
  options,
  text,
  isSelected,
  onSelectOption,
  currentIndex,
}) => {
  const [optionIndexSelected, setOptionIndexSelected] = useState(false);
  const [selectedOption, setSelectedOption] = useState(false);
  const classes = useStyles();

  const selectCard = (index, option) => {
    setOptionIndexSelected(index);
    setSelectedOption(option);
  };

  const optionBoxClasses = classNames({
    [classes.optionBox]: true,
    [classes.selectedOptionBox]: selectedOption,
  });

  return (
    <Container>
      {isSelected && (
        <>
          <div className={classes.question}>
            {selectedOption ? (
              <CheckBoxIcon color="primary" />
            ) : (
              <CheckBoxOutlineBlankIcon color="primary" />
            )}
            <Typography
              className={classes.questionText}
              variant="h5"
              component="h2"
            >
              {text}
            </Typography>
          </div>

          <Container className={classes.questionBox}>
            <IconButton
              color="primary"
              disabled={currentIndex === 0}
              aria-label="Anterior"
              onClick={() => onSelectOption(currentIndex - 1)}
            >
              <ArrowBackIosIcon />
            </IconButton>

            <div className={classes.optionsContainer}>
              {options.map((option, i) => (
                <Grid
                  container
                  direction="column"
                  justify="center"
                  alignItems="center"
                  className={
                    i === optionIndexSelected
                      ? optionBoxClasses
                      : classes.optionBox
                  }
                  onClick={() => selectCard(i, option)}
                >
                  {option.option}
                </Grid>
              ))}
            </div>

            <IconButton
              color="primary"
              aria-label="Siguiente"
              disabled={!selectedOption}
              onClick={() => onSelectOption(currentIndex + 1, selectedOption)}
            >
              <ArrowForwardIosIcon />
            </IconButton>
          </Container>
        </>
      )}
    </Container>
  );
};

export default Questions;
