import { Link } from "react-router-dom";
import { Fab, makeStyles } from "@material-ui/core";
import CardGiftcardOutlinedIcon from "@material-ui/icons/CardGiftcardOutlined";
import SportsEsportsIcon from "@material-ui/icons/SportsEsports";
import React from "react";

const useStyles = makeStyles((theme) => ({
  notificationBox: {
    position: "fixed",
    zIndex: 1,
  },
  notificationIcon: {
    margin: "5px 5px",
  },
  link: {
    textDecoration: "none",
  },
  notificationText: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));

const Notification = ({ text, url, color = "primary", type, onClick }) => {
  const classes = useStyles();
  return (
    <div className={classes.notificationBox}>
      <Link to={url} className={classes.link}>
        <Fab variant="extended" color={color} onClick={onClick}>
          {type === "GAME" ? (
            <SportsEsportsIcon />
          ) : (
            <CardGiftcardOutlinedIcon className={classes.notificationIcon} />
          )}
          <span className={classes.notificationText}>{text}</span>
        </Fab>
      </Link>
    </div>
  );
};

export default Notification;
