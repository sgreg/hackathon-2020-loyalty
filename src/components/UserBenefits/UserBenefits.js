import { Grid, makeStyles, Typography } from "@material-ui/core";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import React, { useState } from "react";
import DisplayItem from "../DisplayItem/DisplayItem";
import useUserBenefits from "../../hooks/useUserBenefits";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 16,
  },
  header: {
    backgroundColor: "#fff",
    display: "flex",
    padding: "16px 16px",
    justifyContent: "space-between",
    alignItems: "center",
    cursor: "pointer",
  },
  content: {
    padding: 16,
    backgroundColor: "#fff",
    marginTop: 8,
  },
  gridItem: {
    flexGrow: 1,
  },
}));

const UserBenefits = ({ userBenefits }) => {
  const [showBenefits, setShowBenefits] = useState(true);

  const classes = useStyles();

  const toggleShowBenefits = () => {
    setShowBenefits(!showBenefits);
  };

  return (
    <div className={classes.root}>
      <div className={classes.header} onClick={toggleShowBenefits}>
        <Typography variant="h5">Mis beneficios</Typography>
        <KeyboardArrowDownIcon />
      </div>
      {showBenefits && (
        <div className={classes.content}>
          <Grid container spacing={2}>
            {userBenefits.map((benefit, index) => {
              return (
                <Grid
                  item
                  sm={12}
                  lg={6}
                  className={classes.gridItem}
                  key={`user-benefit-item-${index}`}
                >
                  <DisplayItem item={benefit} />
                </Grid>
              );
            })}
          </Grid>
        </div>
      )}
    </div>
  );
};

export default UserBenefits;
