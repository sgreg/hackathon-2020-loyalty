import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./pages/dashboard/Dashboard";
import QuizPage from "./pages/quiz/Quiz";
import { CssBaseline } from "@material-ui/core";
import Layout from "./components/Layout/Layout";

const App = () => {
  return (
    <Router>
      <Layout>
        <CssBaseline />
        <Switch>
          <Route exact path="/">
            <Dashboard />
          </Route>
          <Route path="/quiz">
            <QuizPage />
          </Route>
        </Switch>
      </Layout>
    </Router>
  );
};

export default App;
